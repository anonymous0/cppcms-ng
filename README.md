CppCMS-ng is forked project from original CppCMS.

Reasons:

0. **I do not want wait years** for upstream. https://sourceforge.net/p/cppcms/mailman/message/35459746/
1. **Migration to C++11.** Or to C++14. Upstream instead want to support C++03.
2. **Drop some booster classes**, and using pure STL and new language features. For example, migrate to std::thread, smart-pointers, etc. I don't see reasons to keep own re-invented things.
3. **Clean-up code, make refactoring**, make it more robust, cleaner and faster (rvo, constexpr, lambdas, syntax sugar).
4. **Completely drop old and deprecated stuff.**
5. **Migrate to git.**
6. **Use more user-friendly and more ethical hosting than SourceForge.**